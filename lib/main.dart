import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_fonts/google_fonts.dart';

import 'app/routes/app_pages.dart';

ThemeData _buildTheme(brightness) {
  var baseTheme = ThemeData(brightness: brightness);

  return baseTheme.copyWith(
    textTheme: GoogleFonts.latoTextTheme(baseTheme.textTheme),
  );
}

void main() async {
  EasyLoading.instance
    ..indicatorType = EasyLoadingIndicatorType.fadingGrid
    ..contentPadding = const EdgeInsets.all(16)
    ..maskType = EasyLoadingMaskType.clear
    ..dismissOnTap = false;

  await GetStorage.init();

  runApp(
    GetMaterialApp(
      title: "Application",
      initialRoute: AppPages.INITIAL,
      getPages: AppPages.routes,
      theme: _buildTheme(Brightness.light),
      builder: EasyLoading.init(),
    ),
  );
}
