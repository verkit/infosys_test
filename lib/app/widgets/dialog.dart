import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

showPopup(
  String title,
  String message, {
  void Function()? onPressed,
  void Function()? onCancel,
}) {
  EasyLoading.dismiss();
  Get.dialog(
    CupertinoAlertDialog(
      title: Text(title),
      content: Text(message),
      actions: <Widget>[
        TextButton(
          onPressed: () => onCancel ?? Get.back(),
          child: const Text('Cancel'),
        ),
        TextButton(
          onPressed: () => onPressed ?? Get.back(),
          child: const Text('OK'),
        ),
      ],
    ),
  );
}
