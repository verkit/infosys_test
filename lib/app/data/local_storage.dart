// ignore_for_file: non_constant_identifier_names

import 'dart:convert';

import 'package:get_storage/get_storage.dart';
import 'package:infosys/app/modules/login/user_model.dart';

abstract class _LocalKeys {
  static String USER = 'user';
}

class LocalStorage {
  static User? get currentUser {
    final box = GetStorage();
    String? val = box.read(_LocalKeys.USER);
    return val != null ? User.fromJson(jsonDecode(val)) : null;
  }

  static saveUser(User value) {
    final box = GetStorage();
    return box.write(_LocalKeys.USER, jsonEncode(value.toJson()));
  }

  static logout() {
    final box = GetStorage();
    return box.erase();
  }
}
