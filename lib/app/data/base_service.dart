import 'package:get/get.dart';
import 'package:infosys/app/data/local_storage.dart';
import 'package:infosys/app/widgets/dialog.dart';

class BaseService extends GetConnect {
  @override
  void onInit() {
    var user = LocalStorage.currentUser;

    httpClient.baseUrl = "https://dummyjson.com";
    httpClient.defaultContentType = "application/json";
    httpClient.timeout = const Duration(seconds: 8);

    var headers = {'Authorization': "Bearer ${user?.token}"};
    httpClient.addAuthenticator<Object?>((request) async {
      request.headers.addAll(headers);
      return request;
    });

    super.onInit();
  }
}

errorHandler(Response response) {
  var error = _responseText(response);
  showPopup('Terjadi kesalahan', error);
}

String _responseText(Response response) {
  switch (response.statusCode) {
    case 200:
    case 201:
    case 202:
      var responseJson = response.body.toString();
      return responseJson;
    case 400:
      return response.statusText ?? "Server Error pls retry later";
    case 403:
      return response.statusText ?? 'Error occurred pls check internet and retry.';
    case 500:
    default:
      return 'Error occurred retry';
  }
}
