import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/splashscreen_controller.dart';

class SplashscreenView extends GetView<SplashscreenController> {
  const SplashscreenView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GetBuilder<SplashscreenController>(
      init: controller,
      builder: (ctrl) {
        return Scaffold(
          body: Stack(
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: Image.asset('assets/header-splash.png'),
              ),
              Center(
                child: Image.asset('assets/logo.png'),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Image.asset('assets/footer-splash.png'),
              ),
            ],
          ),
        );
      },
    );
  }
}
