import 'package:get/get.dart';
import 'package:infosys/app/data/base_service.dart';

import '../controllers/splashscreen_controller.dart';

class SplashscreenBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SplashscreenController>(
      () => SplashscreenController(),
    );

    Get.put<BaseService>(BaseService(), permanent: true);
  }
}
