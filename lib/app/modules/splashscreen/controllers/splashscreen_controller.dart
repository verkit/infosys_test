import 'package:get/get.dart';
import 'package:infosys/app/data/local_storage.dart';
import 'package:infosys/app/routes/app_pages.dart';

class SplashscreenController extends GetxController {
  @override
  void onInit() {
    Future.delayed(const Duration(seconds: 2), () {
      if (LocalStorage.currentUser != null) {
        return Get.offNamed(Routes.HOME);
      }
      return Get.offNamed(Routes.LOGIN);
    });
    super.onInit();
  }
}
