import 'package:get/get.dart';
import 'package:infosys/app/modules/home/providers/product_provider.dart';

import '../controllers/home_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(
      () => HomeController(),
    );

    Get.put(ProductProvider());
  }
}
