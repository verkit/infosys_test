import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:infosys/app/data/local_storage.dart';
import 'package:infosys/app/routes/app_pages.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple[600],
        foregroundColor: Colors.white,
        title: const Text('HomeView'),
        centerTitle: true,
        actions: [
          IconButton(
            onPressed: () {
              LocalStorage.logout();
              Get.offNamed(Routes.LOGIN);
            },
            icon: Icon(Icons.logout),
          )
        ],
      ),
      body: Obx(() {
        return controller.isLoading.value
            ? Center(
                child: CircularProgressIndicator(),
              )
            : ListView.builder(
                itemCount: controller.items.length,
                itemBuilder: (ctx, i) {
                  var item = controller.items[i];
                  return ListTile(
                    title: Text(item.title!),
                    leading: Image.network(item.thumbnail!),
                  );
                },
              );
      }),
    );
  }
}
