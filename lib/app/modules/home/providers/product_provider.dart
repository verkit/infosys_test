import 'package:infosys/app/data/base_service.dart';

import '../product_model.dart';

class ProductProvider extends BaseService {
  Future<List<dynamic>> getProducts({
    int? limit,
    int? skip,
  }) async {
    final response = await get(
      '/products',
      query: {
        'limit': limit,
        'skip': skip,
      },
    );

    // var productRaw = ;
//
    // List<Product> items = productRaw.map((item) => Product.fromJson(item)).toList();

    return response.body['products'];
  }

  Future<List<Product>?> searchProducts({
    required String text,
    int? limit,
    int? skip,
  }) async {
    final response = await get(
      '/products/search',
      query: {
        'q': text,
        'limit': limit,
        'skip': skip,
      },
    );
    return response.body;
  }

  Future<List<Product>?> getProductsByCategory({
    required String category,
    int? limit,
    int? skip,
  }) async {
    final response = await get(
      '/products/category/$category',
      query: {
        'limit': limit,
        'skip': skip,
      },
    );
    return response.body;
  }

  Future<Product?> getProduct(int id) async {
    final response = await get('/products/$id');
    return response.body;
  }
}
