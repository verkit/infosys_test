import 'package:get/get.dart';
import 'package:infosys/app/modules/home/product_model.dart';
import 'package:infosys/app/modules/home/providers/product_provider.dart';

class HomeController extends GetxController {
  final ProductProvider _productProvider = Get.find();
  final RxList<Product> items = <Product>[].obs;
  final isLoading = false.obs;
  @override
  void onInit() async {
    isLoading.value = true;
    await _productProvider.getProducts().then((value) {
      items.value = value.map((item) => Product.fromJson(item)).toList();
      isLoading.value = false;
    });
    super.onInit();
  }
}
