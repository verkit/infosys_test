import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:infosys/app/data/local_storage.dart';
import 'package:infosys/app/modules/login/providers/user_provider.dart';
import 'package:infosys/app/routes/app_pages.dart';
import 'package:infosys/app/widgets/dialog.dart';

class LoginController extends GetxController {
  final UserProvider _provider = Get.find();
  final formKey = GlobalKey<FormState>();
  final username = TextEditingController().obs;
  final password = TextEditingController().obs;

  login() async {
    EasyLoading.show();
    var login = await _provider.login(username.value.text, password.value.text);

    if (login != null) {
      EasyLoading.dismiss();
      LocalStorage.saveUser(login);
      Get.offNamed(Routes.HOME);
    } else {
      showPopup('Login failed', 'Your username/password is wrong');
    }
  }

  @override
  void onClose() {
    username.value.dispose();
    password.value.dispose();
    super.onClose();
  }
}
