import 'package:get/get.dart';
import 'package:infosys/app/modules/login/providers/user_provider.dart';

import '../controllers/login_controller.dart';

class LoginBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LoginController>(
      () => LoginController(),
    );

    Get.put<UserProvider>(UserProvider());
  }
}
