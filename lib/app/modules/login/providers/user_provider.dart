import 'dart:convert';

import 'package:infosys/app/data/base_service.dart';

import '../user_model.dart';

class UserProvider extends BaseService {
  @override
  void onInit() {
    httpClient.defaultDecoder = (map) {
      if (map is Map<String, dynamic>) return User.fromJson(map);
      if (map is List) return map.map((item) => User.fromJson(item)).toList();
    };
    super.onInit();
  }

  Future<User?> login(
    String username,
    String password,
  ) async {
    var res = await post(
      '/auth/login',
      {
        'username': username,
        'password': password,
      },
    );

    if (res.statusCode == 200) {
      return User.fromJson(jsonDecode(res.bodyString!));
    }

    throw errorHandler(res);
  }
}
