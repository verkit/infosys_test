## 0.0.1+58

Last Merge: !702

### Development Updates

- Create denied forever location access permission status bottom sheet & make parameterized save
  location process
- Integrate GPS button on search_address module with getCurrentLocation method
- Add missing i18n key on Info Toast for pickup cancel order with 'Ambil di Loket' refund type

### Bug Fixes

- Transfer
    - Confirmation page: Wrap confirmation content from sizedBox widget to SingleChildScrollView
      widget to prevent overflowed content.
    - Fix warning handler and remove unnecessary loading on inquiry transfer step 2
    - Fix transfer method UI style
- fix all error take photo image size too large

## 0.0.1+57

Last Merge: !698

### Bug Fixes

- fix sorting menu with null safety
- change shimmer color to transparent
- fixed bug when face recognition on relink

## 0.0.1+56

Last Merge: !696

### Development Updates

- Delivery order - Add Info Toast for pickup cancel order with 'Ambil di Loket' refund type
- Implement theme color app

### Bug Fixes

- Favorit General: Fix still get favorite list data when scrolling

## 0.0.1+55

Last Merge: !693

### Development Updates

- SSL PINNING
    - [PENTEST]implement ssl pinning
- Transfer
    - Add new function integrated with new api to check limit maximum and minimum transfer by
      selected transfer method

### Bug Fixes

- Onboarding
    - Fix data onboarding not updated in cache

## 0.0.1+54

Last Merge: !686

### Development Updates

- Home Screen
    - Add UI near location when location permission not granted
- Register
    - [PENTEST] Add error code handler when create password error from defect pentest

### Bug Fixes

- Home Screen
    - Change linter const to warning
- Login
    - Fix error handler when login
- Mutation
    - Fix date picker range mutation
    - Get message range date 1 year
- Wesel
    - Change wesel confirmation with component
- Relink and Register
    - Deleted copy account number feature in relink and register
- Account Number Card
    - Fix account number not completely showing at home screen

## 0.0.1+53

Last Merge: !683

### Development Updates

- Menu
    - Integration menu highlight
    - Change product name and upgrade account to Overflow Ellipsis

- Order Delivery
    - [UAT] Show empty state if delivery status data is empty on order_delivery_transaction_detail &
      check_delivery_order module
    - Add cancel reason field on order_delivery_transaction_detail module
    - Add expired date field on order_delivery_transaction_detail module

- Profile (Contact Us)
    - Implement integrate image on contact us from portal

- Component Confirmation
    - Add confirmation button text parameter
    - Make source or destination account to be nullable

- Alice
    - Setup new Env for ALICE
    - Alice will not encrypt response and request from API

### Bug Fixes

- Confirmation Component
    - Add title parameter on confirmation component to handle different title in every biller
      confirmation view
    - Remove posReceiptWidget and add contentReceipt Widget for more dynamic content in confirmation
      view component
    - Create new IButton.bottomNavigationBar
    - Use IButton.bottomNavigationBar to replace all existing transfer bottom navigation bar
    - Fix confirmation view in transfer

- Transfer
    - Fix show list bank parameter

- Order Delivery
    - Hide dimensions on document type of goods section (order_delivery_form module)

- Tarik Tunai
    - fix Confirmation on features tarik tunai
    - fix button buttom base screen on features tarik tunai

## 0.0.1+52

Last Merge: !675

### Development Updates

- Encrypt request and response api
- Implementation RASP
- New menu entertaintment
- Order Delivery
    - Add courierNotes field into create address data mapping
    - [UAT] Change address section layout on order_delivery_form module
    - [UAT] Change address section layout on order_delivery_confirmation module
    - [UAT] Change address section layout on order_delivery_transaction_detail module
    - [UAT] Change address section layout on check_delivery_fee & check_delivery_order module
    - [UAT] Add reset button on select pickup schedule section
    - Set PickUpScheduleList Bottom Sheet max height's to screen height divided with 1.5
    - [UAT] Add Info Toast for cancel order & adjust cancel order pickup's message on
      order_delivery_transaction_detail module
    - [UAT] Add Info Toast for setting saved address list
    - Replace hardcoded colors with the one in Palette
    - Replace the regular SizedBox used to create space with sbWidth & sbHeight
    - [UAT] Add Info Toast for setting saved goods list
    - [UAT] Integrate list goods type with system param services
    - [UAT] Integrate list payment method with system param services
    - [UAT] Integrate list cancel reason with system param services
    - [UAT] Migrate list refund method to system param services

### Bug Fixes

- History
    - Fix history componen global
    - Remove scrollcontroller from widget HistoryBaseGlobalView
    - Adding language
- Registration
    - Add validation error code when user validate phone number
    - Change typo text in camera widget
    - Fix upgrade account OTP countdown doesn't update after back to phone number view
- Transfer
    - Fix history transfer blank screen affected by new component update, Fix transfer destination
      card style, Add transaction confirmation view component
- Cek Ongkir
    - Fix Typo redaction activation COD bottomsheet, Reintegrate check delivery process with
      order_delivery_service_type module, add missing validation
- Wesel
    - Fix bug wesel favorite not update before refresh page
- Internet
    - Fix change show modal bottom sheet to showSearchBottomSheet on inquiry internet and favorite
      history internet
    - Fix UI confirmation Internet
    - Fix translate payment internet paymentnewinternet
- Credit Card
    - Fix button bottom base screen
    - Fix change show modal buttom sheet to showSearchBottomSheet
    - Fix UI confirmation credit card

## 0.0.1+51

Last Merge: !662

### Development Updates

- Home
    - Add refresh indicator
- PIN
    - Add animation when user get an error in mpin
    - Layouting new UI
- Migrate Skeleton to IShimmer
- Device feature support, merupakan fungsi aplikasi untuk dapat mengakses fitur-fitur device mobile
  phone seperti kelender untuk fungsi reminding saat promosi berlangsung dan fitur maps untuk
  menunjukkan letak promosi berlangsung
- Order Delivery
    - Integrate list order delivery type with system param services
    - Move custom switch to app/component directory and renamed it with ISwitch

### Bug Fixes

- Order Delivery
    - Add courierNotes field into create address data mapping
    - [UAT] Change address section layout on order_delivery_form module
    - [UAT] Change address section layout on order_delivery_confirmation module
    - [UAT] Change address section layout on order_delivery_transaction_detail module
    - [UAT] Change address section layout on check_delivery_fee & check_delivery_order module
    - [UAT] Add reset button on select pickup schedule section
    - Set PickUpScheduleList Bottom Sheet max height's to screen height divided with 1.5
    - [UAT] Add Info Toast for cancel order & adjust cancel order pickup's message on
      order_delivery_transaction_detail module
    - [UAT] Add Info Toast for setting saved address list
    - Replace hardcoded colors with the one in Palette
    - Replace the regular SizedBox used to create space with sbWidth & sbHeight
- Wesel
    - Add validation for wesel pin when balance not eligible

## 0.0.1+50

Last Merge: !653

### Development Updates

- Order Delivery
    - [UAT] Set language parameter to id on all used GoogleMaps API
    - [UAT] Add commas on address request of getListPostalCode service
    - [UAT] Add courier notes to saved_address_entity
    - [UAT] Implement pagination & more advance search process on saved_address_list module
    - [UAT] Implement pagination & more advance search process on saved_goods_list module
- Common Asset
    - Migrate iconKey to menuItemId for getting icon image

### Bug Fixes

- Order Delivery
    - [UAT] Reset selected service type value after changing insurance input section to avoid
      inaccurate insurance value
    - [UAT] Integrate order_delivery_address_form module with contact picker helper
    - [UAT] Hide dimensions on document type of goods list
    - [UAT] Make the slidable in the address & goods list only 1 open at a time
    - [UAT] Change description label of NearestPostOfficesBottomSheet & AddressOutOfRangeBottomSheet
- Registration
    - Change wording bottom sheet validation when verify data KTP
- History Transaction Base
    - Fix prevent fetching data again if data is not empty and pagination in history general
- Base API :
    - Fix double popup session time out
- Profile
    - [UAT] Fix typo titleEmptyDataDeliveryFee on ID i18n [28]
    - [UAT] Fix blank screen on contact us on empty state [91]

## 0.0.1+49

Last Merge: !645

### Development Updates

- Wesel
    - add nominal validation for wesel inquiry
    - add auto fill wesel inquiry field when trasanction again
- Order Delivery
    - Replace main button on order_delivery_form & order_delivery_cancellation module from gradient
      to primary
    - Make submitted goods data saved by requests
    - Add KAP & PIN field on order_delivery_transaction_detail module
    - Add goods dimension acronym on dimensions input prefix

### Bug Fixes

- Transfer
    - Remove static image and use transaction icon in add to favorite bottom sheet
- Delivery Fee
    - fix Empty statement

## 0.0.1+48

Last Merge: !642

### Development Updates

- Transfer
    - Add local check on insufficient balance in inquiry transfer
    - Disable confirmation button if required field or selected field is not complete in inquiry
      transfer
- Login
    - add error handler when login biometric in unregistered device id
- Component
    - EmptyState UI: Update bottomNavigatorBar

### Bug Fixes

- Fix add gap in detail receipt
- Fix menu when error it will return SizedBox()
- Fix bug activation COD freeze on PIN (null response entity)

## 0.0.1+47

Last Merge: !615

### Development Updates

- Order Delivery
    - Remove insurance information section
    - Make selected goods type value default to first item on list
    - Make all the default input on order_delivery_goods_form module empty
- Component
    - Textfield Decimal: Allows commas in ITextField.decimal to accommodate iOS keyboards
    - IImage: Fix error mounted

### Bug Fixes

- fix handle empty data from portal on contact us and delivery fee
- Withdrawal
    - change icon warning inquiry tarik tunai
    - change name title verify pin (from MPIN to PIN)
- Favorite
    - Fix loading widget when get favorite general data
- Transfer: Prevent get list bank fetching api if data is not empty in list bank bottom sheet
  component
- UAT Registration, Migration, Upgrade Account
    - remove height on all button
    - change input name format in register, upgrade account, and migration account
    - remove validation when user want to navigate back in registration (just for UAT only and need
      confirmation from BA after UAT)

## 0.0.1+46

Last Merge: !627

### Development Updates

- Component (Filter Search History)
    - Add safe area to wrap filter search widget component
- Component (Button)
    - Change default color of IButton.custom to primaryColor
- Component (Search Bottom Sheet)
    - Enable customization on hint and itemWidget in showSearchBottomSheet
- PIN
    - Clear PIN input after getting wrong PIN or other error on PIN module
- Favorite
    - Integrate favorite menu tab data with content param services
    - Add pull to refresh in favorite base global view component
    - Disable edit button favorite if name alias is empty
- Contact Us
    - Make empty state appear if the data is empty

### Bug Fixes

- Tarik Tunai
    - Fix 'Lanjut' button on Tarik Tunai features
- Transfer
    - Fix unnecessary padding in confirmation view transfer and use default padding from detail
      receipt component
    - Fix confirmation button size in confirmation view transfer
    - Fix some padding in inquiry step two transfer view
    - Fix keyboard take to much space above in inquiry transfer step 2 view
    - Fix bug nominal form is missing when doing transaction again
    - Fix size and fit style in bank icon component, and icon destination card in confirmation
      transfer
    - Fix search result not updating in bottom sheet bank list in iOS device
- Location Permission
    - Fix behavioral bug on closing location permission requests dialog
- Upgrade Account
    - Fix error handler upgrade account when verify user data
- Kurlog
    - Fix address suggestion is not complete by remove param type on Google Maps autocomplete
      services
- Favorite
    - Fix favorite menu fetching data again when switching back to the tab
- Internet Availability Checker
    - Fix internet connection availability dialog and it's behavior

## 0.0.1+45

Last Merge: !616

### Development Updates

- Portofolio
    - Integrate with change account name alias services
- Term and Condition
    - Make the 'Agree' button active when the user has scrolled the terms and conditions page to the
      bottom

### Bug Fixes

- IImage Widget
    - Fix IImage didn't update when change from error image
- Receipt & Favorite
    - Fix dynamic biller icon in receipt and favorite
- Kurlog
    - Fix itemtypeid value on check_delivery_fee module
    - Fix shipperzipcode & receiverzipcode value on getDeliveryFee module
    - Fix the location pin not being able to be moved by setting the suggestion result widget
      correctly

## 0.0.1+44

Last Merge: !607

### Development Updates

- Make confirmation button disable on search suggestion address mode
- Implement show maps apps on tap of Near POS offices item on home, register, and uppgrade account

### Bug Fixes

- Fix next button position in transfer inquiry page
- Fix keyboard don't dismiss when touch the outside the form field inside base screen component

## 0.0.1+43

Last Merge: !603

### Development Updates

- remove unused import package in mutation_view.dart
- Add validation for select upgrade account type
- add a check balance function from the backend to check whether the balance is sufficient or not to
  make a transfer

### Bug Fixes

- Check for null on OrderDeliveryInquiryResponseEntity.fromJson to avoid exception during expected
  failed inquiry
- fix widget image minio not update when value change
- Fix upgrade account bug when load account data
- fix blank screen when routing to manage account

## 0.0.1+42

Last Merge: !595

### Development Updates

- add balance checker function in wessel
- Add event redirect to store for rating apps
- Change response error from snackbar to bottom sheet
- Remove border right side Rp in ITextFieldCurrencyNew
- Change search address suggestion result layout to full screen mode

### Bug Fixes

- fix error message logic in nominal input form in wessel
- fix bank icon in show list bank bottom sheet
- Fix base api response error

## 0.0.1+41

Last Merge: !591

### Development Updates

- implement Download PDF Mutation
- Improve response API 401 with title and message
- Change response error from snackbar to bottom sheet

### Bug Fixes

- adding isloading from initpage mutation view model
- Set keyboardDismissBehavior with ScrollViewKeyboardDismissBehavior.onDrag to dismissing keyboard
  on iOS platform at order_delivery_address_form, order_delivery_form, and order_delivery_goods_form
- Change wording on success or failed cancel order process bottom sheet's

## 0.0.1+40

Last Merge: !586

### Bug Fixes

- add sort onboarding function
- fix image error cannot shown
- Fix missing handle s3 assets from http to all string except value startsWith('assets')
- fix favorite and download background button color in mini receipt component
- fix empty widget in history
- fix empty widget in mutation
- fixing language empty widget in mutation
- fix all registration still filled after session expired
- Fixed bug data user not show when success upgrade account

## 0.0.1+39

Last Merge: !579

### Development Updates

- Order Delivery
    - Integrate with get list payeeCode on order_delivery_form module
    - Integrate with get list refund method on order_delivery_cancellation module
- Integration asset with minio aws S3
- Improve UI design in face recognition
- remove empty error bottom sheet from save transaction favorite view model and use i mapping error
  component instead

### Bug Fixes

- Order Delivery: Check null on raw address before splitting into zip code
- Fix before login menu length
- Fix header before login menu
- Add dummy promo content before login menu
- Refresh auth cookies and refresh in init apps
- Fixed bug in upgrade account when input wrong pin
- fix keyboard dismiss when scrolling in wesel inquiry
- fix typo language when success edit favorite

## 0.0.1+38

### Bug Fixes

- Kurlog
    - Fix input decimal on check ongkir
- Linter
    - Remove all use_build_context_synchronously
- Receipt
    - Fix download on Share button not working
- Verify PIN
    - Add new coming soon feature bottom sheet on setup new PIN button clicked after getting
      transaction PIN blocked
    - Remove late init on verifyPINViewArguments to avoid LateInitializationError
- Order Delivery
    - Integrate 'Order Kiriman' features with COD availability services
        - Fix insurance rate appearance & insurance value counting process
        - Add error color on goods weight & dimensions input
        - Make selectedGoodsType initialization to be nullable
        - Remove late init on all arguments variable of order_delivery module to avoid
          LateInitializationError
        - Add missing i18n on getTitleOrderDeliveryType
- Wesel
    - Fix bug overflowed UI in wesel
- Upgrade Account
    - Add new mapping response message
- Transfer
    - Fix mpin view in transfer
- Tarik Tunai
    - Fixing response code wrong pin on confirmation tarik tunai
- Detail Transaksi
    - Implement service API generate PDF
    - Download generate PDF
    - Fixing bug icon search
    - Fixing safe area bottomModal lihat resi
    - Fixing change status in history after back

## 0.0.1+37

### Development Updates

- Update i18n for reciept response

### Bug Fixes

- Order Delivery
    - Add missing enumOrderDeliveryType on navigate to OrderDeliveryCancellationView
    - Remove unnecessary onBack handling on order_delivery_form module
    - Create handling for wrong transaction PIN & blocked transaction PIN and integrate it with
      order_delivery_confirmation & order_delivery_cancellation module
- Profile
    - Add i18n for receipt from BE
    - Change button logout
    - Fix scroll behaviour on profile
    - Fix header on profile
- Receipt
    - Fix favorite icon and share icon in pos summary receipt and pos receipt to match the latest
      design
- Kurlog / Order Kiriman
    - Fix Cek Ongkir - data yang sudah terinput tidak terdispose
    - Fix Delivery order - Setelah melakukan cek ongkir dan melanjutkan pengiriman untuk memilih
      pickup atau drop off input password (login) tidak ada
- Home Screen
    - Fix dashboard error after logout
- Tarik Tunai
    - Fix transaction again on features tarik tunai
    - Fix i18n saldo tidak mencukupi

## 0.0.1+35

### Development Updates

- Update i18n for reciept response

### Bug Fixes

- Favorite
    - Fix duplicate list favorite
    - Remove withdrawal sub tab in transaction tab favorite menu
    - Fix transaction status in pos summary receipt
    - Fix tab name in favorite menu
    - Fix language when favorite list is empty
    - Fix favorite icon when icon is empty
- Transfer
    - Fix inquiry step 2 can do inquiry even the transfer method not selected
    - Fix null error on transfer tab view and add dispose function to tabController
- Portofolio
    - Fix show balance account
- Order Kiriman
    - Fix Saat input MPIN muncul bottom sheet Aktivasi biometric berhasil ketika biometric setting
      belum aktif
    - Fix hide button order kiriman on check ongkir before login when first install or must relink
    - Fix wording in confirmation and receipt
    - Fix summary onBack function
    - Fix Button on summary page not suitable
    - Add error text on goods information section on goods name is empty
    - Add separator on currency input of insurance section
    - Add action to change order delivery type on tap of Address Out of Range Bottom Sheet's main
      button
- Check Receipt
    - Fix Denied Permission Camera saat scan barcode pop up permission tidak hilang
    - Fix Riwayat Cek resi terbaru terletak di list paling bawah
    - Fix Riwayat Cek Resi harusnya hanya menampilkan 3 pencarian terakhir
- Upgrade Account
    - Fix Saat selesai upgrade account dari lite plus ke reguler, di homescreen yang muncul masih
      GIROPOS LITE PLUS

## 0.0.1+34

### Development Updates

- Changes implement new tipography on History transaction
- Update i18n for reciept response

### Bug Fixes

- Portofolio:
    - Integration show balance and set as default (primary)
- Mutation :
    - Fix response and request sevice API mutasi
    - Fix bug wording startdate and enddate
- Scan Receipt :
    - Fix permission for allow camera when deny
- Register/upgrade account :
    - Fix Fitur Camera View ketika permission "Denied & dont ask again" jadi buffer
- Delivery Order:
    - Create bottom sheet for handling 4101 error (Insufficient Balance) on order delivery inquiry
      services
    - Add rebuild process on text field to activate validation on order_delivery_address_form &
      order_delivery_goods_form module
    - Add package warning section & change main button text on order_delivery_service_type module
    - Make goods weight input compatible with decimals
    - Fix order_delivery_confirmation_view's AppBar title on pickup delivery
    - Make order_delivery_transaction_detail refresh after getting result from
      order_delivery_cancellation module
    - Add error text on address section on address is not completed
    - Make the Postal Code list bottom sheet appeared when there's no error
- Tarik Tunai
    - Fix style font & fixing new design tarik tunai

## 0.0.1+33

### Development Updates

- Changes and deprecated for all unused textStyle
- Create nearby_pos_offices_list and integrate with get near POS offices service to show all of the
  POS office nearby (Order Delivery)

### Bug Fixes

- Transfer :
    - Remove search field in transfer purpose bottom sheet
    - Fix favorite transfer list icon
    - Fix insufficient balance check (add fee) in transfer
- Migration :
    - Fix wording error validation
    - Solve use_build_context_synchronously
    - Change deprecated style in i_dialog
- Favorite :
    - Fix wording in bottom sheet when successfully save new favorite
    - Fix wording in favorite view when favorite list is empty
    - Fix wording in bottom sheet when successfully edit favorite
    - Add wesel instant tab inside transfer tab in favorite menu
    - Fix wording in bottom sheet when successfully delete favorite
- Mutation :
    - Fixing mutation list
    - Fixing integrasi mutation
    - Fixing pagination list
    - Fixing search list
    - Fixing bug share receipt
    - Fixing bug datepicker
    - Fixing bug double data on history tab order
- Layout :
    - Fix layout overflow inside balance card
    - Fix height of greeting widget
- Scan Receipt :
    - Fix ui bottomsheet for allow camera
- Register/upgrade account :
    - Add back redaction UI in register reguler account

## 0.0.1+32

### Bug Fixes

- Fix bug UI on Tabbar and fix bug biometric
- Transfer :
    - fix context handler in every async function in transfer code
    - fix insufficient balance handler in transfer
- Homescreen :
    - Fix state scroll behavior when changing tab menu
    - Change color inactive bottom bar
    - Changes all route HomeAfterLoginView to DashboardView
- Fix error otpReference migration bottom sheet success in verif account data
- Fix permission camera on scan barcode & show alert upload not qr image (Scan Resi)
- Order Delivery :
    - Add missing maximum document weight validation label
    - Make the pickup schedule selectable
    - Add srcAccountNumber field on pickup inquiry service's request body

## 0.0.1+31

### Development Updates

- Portofolio: Change UI edit limit, Add bottomsheet edit account name
- Top up saldo: change widget Image

### Bug Fixes

- Order Delivery
    - Fix statusText value condition on PosSummaryReceiptView's Order Kiriman
    - Fix main button's order_delivery_address_form_view & order_delivery_goods_form_view width
    - Change order_delivery_goods_form & order_delivery_address_form main button's on keyboard
      appear behavior
- Transfer: fix cannot process transfer inquiry to confirmation
- Homescreen:
    - fix image dummy failed load header
    - fix integration upgrade account
    - fix height bottom bar in ios
    - add animated card balance
- Upgrade account: fix bug multiple select account type
- PreLogin: fix biometric logic
- fix phone number format

## 0.0.1+30

### Development Updates

- Add feature upgrade account
- Create search bottom sheet component
- Order Delivery: change service for inquiry of pickup order delivery type

### Bug Fixes

- Transfer
    - fix destination number form not clear after back from inquiry step 2 view
    - move all show list bank bottom sheet from view to view model
- Migration
    - Fix detail account after mpin
    - Fix remove unused key storage on migration
    - Fix missing bypass in flow migration
- Registration: Fix phone number format in registration phone OTP
- fix all show bottom sheet in favorite menu UI to match the design
- fix safearea pin input

## 0.0.1+29

### Bug Fixes

- Fix error back from scan receipt (routing)
- Fix error save local last search receipt
- Fix forceclose when biometric running
- Fix style html on detail merchant, fix scroll on topup pospay
- Fix screen detail pengiriman and new service

## 0.0.1+28

### Development Updates

- Add new blocked feature view
- Topup: Update UI scroll list item Bank
- Change onback validation bottom sheet in registration, relink and migration

### Bug Fixes

- Biometric login wording

## 0.0.1+27

### Development Updates

- Order Delivery: Add error text on address data is incomplete
- Order Delivery: Change layout of pick_up_schedule_list_bottom_sheet_view.dart to accommodate
  pickup-schedule list
- Home Screen: Update scroll behavior in promo section

### Bug Fixes

- Order Delivery: Add save goods data checkbox & fix goods form validation
- Biometric Login: Delete biometric disabled flagging on login
- Check Delivery Fee: Change service '/posajaservice/checkRate/executeCheckRate' to '
  /posajaservice/jenisLayanan/findAll'

## 0.0.1+26

### Development Updates

- Order Delivery: UI Improvement in location pin tooltip, switch asuransi
- Profile: Improvement scroll header
- Implement Minio helper

### Bug Fixes

- Relink: fixed app bar title and fixed UI issue
- Login: fixing validate wrong password
- Kurlog: fix bugs back button on check receipt, UI kurlog move status to under reciever, change
  image done, fix wording label
- Biometric: fix wording and change tnc

## 0.0.1+25

### Development Updates

- Registration :
    - Change UI Form Registration, Relink, Migration
    - Integration condition bypass ekyc in relink and migration
- Integrate transfer with new api
- Order Delivery (Complete Flow & Validation)
- Improve splash screen

### Bug Fixes

- Mutation :
    - fix mutation format date
    - fix route from detail topup
    - fix icon

## 0.0.1+24

### Development Updates

- Home: Hide Feature Request
- Update UI Topup & Mutation

### Bug Fixes

- Fix: button login after session expired
- Registration :
    - fixing issue on registration and relink new devices
    - fixing overflow keyboard otp email
    - update register name field and fix UI issue

## 0.0.1+23

### Development Updates

- Order Delivery: Search Address Google Map style for filling address information form
- Integrate upgrade account, mutation, i18, restructure code portfolio

### Bug Fixes

- Kurlog : Fix validation weight when empty address
- Fix design issue on registration and relink
- Fix screen info email
- Fix OTP validation message
- Change icon and text coming soon
- Fix before login indicator scroll
- Fix accountNumber align left in home
- Update message error in password and email registration

## 0.0.1+22

### Development Updates

- Profile: add version and change icon Security on profile
- Registration: add new condition when relink is empty, migration flow on progress

### Bug Fixes

- Kurlog: validation weight of type goods on Check Ongkir
- Favorite: exclude favorite menu from being attached to dashboard to have independent screen
- Registration: fix button, checker tnc, relink button can't pressed
- fix transaction again receipt and summary receipt
- fixing enabledrag to false check version alert and fixing tipo link check version

## 0.0.1+21

### Bug Fixes

- Fix SafeArea screen
- Fix duplicate route name

## 0.0.1+20

### Development Updates

- Favorite Menu: add to favorite from receipt and from favorite per biller menu
- Profile: change set isActivedBiometric to local storage from profile for login biometric flagging
- Check Version: new screen check version
- Transfer: add Summary Receipt in inquiry
- Favorite: edit and delete favorite feature
- Registration: fix some bugs and add new flow

### Bug Fixes

- Transfer: fix back button error when doing transaction again from favorite
- Asset: fix load asset not null when get an error in backend
- fix transaction again favorite by removing old manual input routeName to dynamic switch routeName
  by Transaction Category
- Fix validation Stuff on CheckDeliveryFeeView

## 0.0.1+19

### Development Updates

- History: redirect to mini receipt
- Check Receipt: Change UI Scan QR
- Onboarding: add default onboarding data
- Portfolio: update UI Portfolio Manage Account, Mutation and upgrade account
- Registration: improve UI registration and migration (not yet mapping redirect to migration)

## 0.0.1+17

### Development Updates

- Order Delivery (Delivery Service Type) : change item layout of delivery service type list
- Check Delivery Fee: integration address with google API on Check Delivery Fee
- Riwayat: Implement new screen filter and riwayat

### Bug Fixes

- Check Delivery fee: fix complicated widget (grey widget)
- Fix-temporary: FCM Token IOS: block hit get FCM Token in ios
- Register: fix registration when completed create pos pin

## 0.0.1+16

### Development Updates

- Add Registration: layouting new design
- Add profile: lauouting, routing, foldering icon, submenu
- Add Order kiriman: integrate change address information layout and integrate order_delivery_form
  module with the new order_delivery_address_form.
- Add Order kiriman: change goods information layout and integrate order_delivery_form module with
  the new order_delivery_goods_form.
- Add Portfolio Upgrade Account: Update UI verification handphone, OTP, verification ktp, address
  and i18n
- Add Homescreen: Add tnc when registration
- Add Homescreen: Integration near office
- Add Homescreen: Favorite menu must filled 4 item
- Add Homescreen: integration block menu, product name
- Add receipt : new view with summary and detail
- Add check delivery: fee UI from line to double chevron

### Bug Fixes

- Top saldo: Fix list source of fund and function copy virtual account
- Transfer: Fix Get the maximum and minimum transfer nominal based on selected transfer method and
  pospay method, error nominal validation in inquiry step two transfer
- Homescreen: Fix duplicate hit API in dashboard, fix clip name no longer than 60% width screen,
  handle close hit twice phone button

## 0.0.1+15

### Development Updates

- Add Transfer Bi Fast to category group when hit transfer history list service
- Scan QR from galery or camera
  Add bottomsheet when get response code 400
  Change to new template UI on Track delivery
  Change routing from Home before login
- UI Portfolio, Mutation,
- UI Upgrade Account until Verification HP on Portofolio
- create order_delivery_cancellation & cancellation_reason_bottom_sheet module and integrate with
  it's services on Order Delivery
- add integration TNC on Profile

### Bug Fixes

- fix nominal validate logic on transfer,
- fix transfer purpose validate error logic on transfer,
- remove string "Rp" from language and use iToRp() instead on transfer,
- change some text style in inquiry step 2 view on transfer
- fixing favorite history paket data and fix button buttombar on features paket data
- fix not showing loading when hit api service in transfer,
- remove default selected transfer method in inquiry step 2 view on transfer,
- fix validate logic function name on transfer,
- remove unused enum in transfer inquiry on transfer.
- fix continue button not working in inquiry step 2 transfer giro pos
- fix home after login & history transaction
  Change entity CheckResiResponseEntity to nullable
- prevent getListOfTransferMethod when doing Pospay transaction, because it's not necessary on
  Transfer
- Cek ongkir ketika pilih alamat drop off, alamat nya harus dipilih dua kali baru bisa terpilih on
  Kurlog
- Ketika mengisi alat awal dan tujuan lalu keluar dari menu cek ongkir, ketika masuk kembali ke menu
  cek ongkir alamat yang sudah diinput sebelumnya masih ada, harusnya terhapus on Kurlog
- fix selected transfer method code from index to class, to handle dynamic list transfer method data
  in the future on Transfer

## 0.0.1+14

### Development Updates

- Add enum for inquiry step type on transfer
- adding new response result list fromJsonToResponseList
- add integration API for update favorite menu
- integrate address CRUD service & order_delivery_service_type module with order_delivery_form
  module.

### Bug Fixes

- Fix: bug fix register email error
- Fix : fixing bug and routing Detail Sender
- Fix : remove unnecessary ref invalidate confirmation provider on transfer
- Fix some transfer variable still exist when re-login in the middle of doing transfer
- Change 'Dimensi' i18n key on check_delivery_fee & track_delivery_order module
- Change i18n on label of form_stuff_widget.dart
- Change IConstant's property name on check_delivery_fee_view_model.dart
- Fix(order_delivery_transaction_detail): fix parsing proses on get transaction history data
- Fix(order_delivery_transaction_detail): fix bottom navigation button layout
- Fix parsing proses on get transaction history data & bottom navigation button layout on
  order_delivery_transaction_detail module
- Fix a typo in the IConstant property call on order_delivery_form_view_model.dart
- make all the entity's property (order_delivery module) nullable
- Fix favorite history and button buttom next inquiry on credit card
- Fix flaging after login for onboarding
- Fix (onboarding) : fixing onboarding repository response entity
- Fix (onboarding) : change end point service onboarding
- update List SOF component
- update logic and response assets API, it will load asset from BE first, then load asset from local
- Fix overflow UI in before login, and change logic when user login
- Fix logic skip onboarding after login
- change transfer purpose the latest design
- Fix error border color when validate nominal and transfer purpose

## 0.0.1+13

### Development Updates

- Order Delivery (Delivery Type Service): Integration API
- Order Delivery (Confirmation): create sender & recipient information section
- Order Delivery (Goods Form): Integration API (Jenis Barang)
- Order Delivery (Transaction Detail): Layouting UI & Integration API
- Registration: Revamp UI
- Track Delivery order: Onprogress UI & Update Integration API
- Transfer: Revamp UI
- HomeScreen: Revamp UI
- Transaction history: Revamp UI & Integration API
- Topup Pospay: Revamp UI & Integration API
- Portofolio: Layouting UI
- Profile: change profile menu & flow activation biometric
- Kurlog: New UI from mapping response from service check Resi / track delivery, add save local last
  search delivery history on check resi

### Bug Fixes

- Registration: Fix UI Bug, Fix bug forgot password, and fix get OTP
- Withdrawal: Fix button inquiry validation
- Internet: Fix button inquiry validation
- Insurance: Fix button inquiry validation
- Pulsa: Fix button inquiry validation
- Login & Register: Remove loading
- Ewallet: Fix button bar in favorite history screen
- Transfer: Fix bug
- fix bug when change password

## 0.0.1+12

### Development Updates

- Top Up Saldo: Revamp UI
- Favorite: add temporary image

### Bug Fixes

- Asset logic read data from local and api
- Onboarding fix asset local cache image
- Add duration in splash screen minimum 2 seconds
- improve asset in before login
- Transfer
    - fix render flex error when inquiry step two in transfer
    - using language for transfer label
    - add transfer again feature in transfer
    - error when get transfer purpose
    - add temporary srcSystemErrorCode when perform mpin posting giro pos
- Profile: toggle switcher
- Check Delivery: fix UI tracking delivery & validation stuff
- Tarik tunai: fix validate button inquiry
- Registration: Fix UI choose OTP
- History: add asset icon

## 0.0.1+11

### Development Updates

- Order delivery (Drop Off): Layouting UI
- Profile: integration of non active biometric, change icon twitter & forgot pin

### Bug Fixes

- Kurlog: fixing route check delivery fee, UI track order from scan & input receipt number

## 0.0.1+10

### Development Updates

- Component halaman favorite untuk transaksi
- Transfer: Revamp UI
- New UI screen filter history
- Onboarding: Revamp UI
- QRIS: Implement & Integration API
- Before Login: Revamp UI
- Check Delivery: Update UI
- Activation Biometric

### Bug Fixes

- Fix route withdrawal

## 0.0.1+9

### Development Updates

- Add font Manrope
- Profile: Integration Contact US
- Profile: add forgot pin, password
- HistoryView: add routing to page lacak resi
- Topup Pospay: Integration API
- Setup FCM
- Tarik Tunai: Integration API
- Contact us: add launcher on feature
- Wesel: Integration API

### Bug Fixes

- HomeScreen: Session expired not logged out, Session expired stuck in buffering ,Balance amount has
  no digits
- Kurlog:
    - Cek Resi Barcode, setelah resi kemudian di back harusnya ke home bukan ke camera view
    - Cek resi Barcode, saat sudah di resi namun kamera masih berada di posisi barcode resi nya
      muncul berkali-kali (device huawei)
    - Cek ongkir, search provinsi keyboard nutupin field input
    - Cek ongkir, keyboard nutupin semua field informasi barang
    - Cek ongkir, button tidak proporsional
    - Cek ongkir, field nilai barang belum sesuai dengan wording info maximum input harganya
    - Cek ongkir, field nilai barang harusnya tidak perlu menampilkan Rp lagi ketika diinput
    - Cek ongkir, saat tampilan estimasi harga sudah muncul kemudian button tutup di tekan harusnya
      direct ke home

## 0.0.1+8

### Development Updates

- Update component such as Receipt, TabBar, Empty State, History List, Alert
- Transfer Giro Pos: For the record, this feature still uses the old design, and ignore some of the
  code that refers to the latest design, because the work is already half way done.
- HomeScreen: Integration API Saldo

### Bug Fixes

- HomeScreen:
    - Fix QRIS icon goes up when filling the receipt check bar
    - Fix Exhausted session not logged out, but the refresh token doesn't handle
    - Fix Menu becomes double after regist lite plus account, more and more when re-login but back
      to normal when force close apk
- Change Pin:
    - Fix after successfully logging out, should still be logged in.
- Forgot Pin:
    - Fix problem occurred when confirming pin
- Change Password:
    - Fix button not enabled when keyboard layout has not changed to symbol / closed
    - Fix exit page from password confirmation but the input password has not been deleted.
- Transfer:
    - Fix Transaction Category when transferring is still wrong so I get a 0019 pop up when entering
      the new destination transfer page.
    - Fix confirmation page & receipt still empty
- Login:
    - Fix Login after relogin from a page other than home should still go to home

## 0.0.1+7

### Development Updates

- Kurlog : Integration with mixin on check delivery fee area field & add localization
- E-Money : Layouting UI

### Bug Fixes

- Registration: Fix verify ocr & add localization

## 0.0.1+6

### Development Updates

- Update Component Checkbox, Receipt, History
- E-wallet: Integration API Service and i18n
  Translate. http://bankjateng.ist.id:9999/projects/pos-superapp/work_packages/345/activity
- Home : Refactor component home
- Credit Card : add translate and integration API
  service. http://bankjateng.ist.id:9999/projects/pos-superapp/work_packages/320/activity
- Paket Data : Loading Integration API Service and i18n
  Translate. http://bankjateng.ist.id:9999/projects/pos-superapp/work_packages/404/activity
- Pulsa : Loading Integration API Service and i18n
  Translate. http://bankjateng.ist.id:9999/projects/pos-superapp/work_packages/292/activity
- Forgot Pin : Feature Ready. http://bankjateng.ist.id:9999/projects/pos-superapp/work_packages/88
- Forgot Password : Feature
  Ready. http://bankjateng.ist.id:9999/projects/pos-superapp/work_packages/89

### Bug Fixes

- Internet: Fix validator and
  UI. http://bankjateng.ist.id:9999/projects/pos-superapp/work_packages/115/activity

## 0.0.1+4

### Development Updates

- Setup asset management
- Insurance : Integration API Service, Routing , and i18n
  Translate. http://bankjateng.ist.id:9999/projects/pos-superapp/work_packages/302
- Registration : Add translation
- Scan Resi : add features integration to API service, fix widget track delivery, and add i18n on
  text widgets
- Login: add dialog when password wrong
- Internet: Integration API Service, Routing Feature
  Internet http://bankjateng.ist.id:9999/projects/pos-superapp/work_packages/394

### Bug Fixes

- OnBoarding Screen : Fixing design from on boarding, and get bg image from
  BE http://bankjateng.ist.id:9999/projects/pos-superapp/work_packages/143
- Registration : add validation for register when verify domisili address, fix UI error on pin,
  verify ktp, stepper
- History Transaction & Favorite Base : fixing color text setting and change size icon favorite